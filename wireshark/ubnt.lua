-- Ubiquiti Networks Discovery Protocol Dissector

ubnt_discover_proto = Proto("ubnt_discover", "Ubiquiti Discovery Protocol")
ubnt_discover_proto.fields.ver      = ProtoField.int8          ("ubnt.ver",       "Version")
ubnt_discover_proto.fields.type     = ProtoField.int8          ("ubnt.type",      "Type")
ubnt_discover_proto.fields.length   = ProtoField.int16         ("ubnt.length",    "Length")
ubnt_discover_proto.fields.hwaddr   = ProtoField.ether         ("ubnt.hwaddr",    "Hardware Address")
ubnt_discover_proto.fields.fwver    = ProtoField.string        ("ubnt.fwaddr",    "Firmware Version")
ubnt_discover_proto.fields.uptime   = ProtoField.relative_time ("ubnt.uptime",    "Uptime")
ubnt_discover_proto.fields.hostname = ProtoField.string        ("ubnt.hostname",  "Hostname")
ubnt_discover_proto.fields.product  = ProtoField.string        ("ubnt.product",   "Product Name")
ubnt_discover_proto.fields.addrhw   = ProtoField.ether         ("ubnt.addr.hw",   "Hardware Address")
ubnt_discover_proto.fields.addripv4 = ProtoField.ipv4          ("ubnt.addr.ipv4", "IPv4 Address")

function ubnt_discover_proto.dissector(buffer, pinfo, tree)
    pinfo.cols.protocol = "UBDP"

    -- Read the packet header
    local version = buffer(0, 1):uint()
    local message = buffer(1, 1):uint()
    local length  = buffer(2, 2):uint()

    local subtree = tree:add(ubnt_discover_proto, buffer())
    subtree:add(ubnt_discover_proto.fields.ver, buffer(0, 1))
    subtree:add(ubnt_discover_proto.fields.type, buffer(1, 1))
    subtree:add(ubnt_discover_proto.fields.length, buffer(2, 2))

    -- Process the data fields
    if length > 0 then
      local offset = 4
      while offset < length do
        -- Extract the header for each field
        local field_id     = buffer(offset, 1):uint()
        local field_length = buffer(offset + 1, 2):uint()
        offset = offset + 3

        -- Hardware Address
        if field_id == 0x1 then
          subtree:add(ubnt_discover_proto.fields.hwaddr, buffer(offset, field_length))
        -- Address
        elseif field_id == 0x2 then
          local mac_address = buffer(offset, 6):ether()
          local ip_address  = buffer(offset + 6, 4):ipv4()
          addresstree = subtree:add(buffer(offset, field_length),
            string.format("Address, HWAddr: %s (%s), IPv4: %s (%s)",
              tostring(mac_address),
              tostring(mac_address),
              tostring(ip_address),
              tostring(ip_address)))
          addresstree:add(ubnt_discover_proto.fields.addrhw, buffer(offset, 6))
          addresstree:add(ubnt_discover_proto.fields.addripv4, buffer(offset + 6, 4))
        -- Firmware Version
        elseif field_id == 0x3 then
          subtree:add(ubnt_discover_proto.fields.fwver, buffer(offset, field_length))
        -- Uptime
        elseif field_id == 0xa then
          subtree:add(ubnt_discover_proto.fields.uptime, buffer(offset, field_length))
        -- Hostname
        elseif field_id == 0xb then
          subtree:add(ubnt_discover_proto.fields.hostname, buffer(offset, field_length))
        -- Product
        elseif field_id == 0xc then
          subtree:add(ubnt_discover_proto.fields.product, buffer(offset, field_length))
        end
        offset = offset + field_length
      end 
    end
end

-- Register our handler
udp_table = DissectorTable.get("udp.port")
udp_table:add(10001, ubnt_discover_proto)
