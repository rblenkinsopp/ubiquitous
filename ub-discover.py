#!/bin/env python
from __future__ import print_function

from collections import namedtuple
from datetime import datetime, timedelta
import struct
import socket
import time

# Try and use the netifaces library
supports_netifaces = False
try:
  import netifaces
  supports_netifaces = True
except ImportError:
  print("Requires netifaces module to correctly scan all interfaces")

# Get the broadcast addresses and associated interfaces
interfaces = []
if supports_netifaces:
  for interface in netifaces.interfaces():
    addresses = netifaces.ifaddresses(interface)
    if netifaces.AF_INET in addresses:
      for address in addresses[netifaces.AF_INET]:
        if "broadcast" in address:
          interfaces.append((interface, address["broadcast"]))
if len(interfaces) == 0:
  interfaces.append(("all", "255.255.255.255"))

UBNT_DISCOVER_PORT = 10001
UBNT_LISTEN_PORT   = 39916
UBNT_DISCOVER_ADDR = ("255.255.255.255", UBNT_DISCOVER_PORT)
UBNT_DISCOVER_MSG  = bytearray([1,0,0,0]);

sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
sock.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
sock.sendto(UBNT_DISCOVER_MSG, UBNT_DISCOVER_ADDR)

fields = {
    "hwaddr":    0x1,
    "address":   0x2,
    "fwversion": 0x3,
    "uptime":    0xa,
    "hostname":  0xb,
    "product":   0xc
}

def print_host_details(host):
  print("hostname: %s" % host.hostname)
  print("hwaddr: %s" % host.hwaddr)
  print("ipv4: %s" % host.ipv4)
  print("product: %s"  % host.product)
  print("fwversion: %s" % host.fwversion)
  uptime = datetime(1,1,1) + timedelta(seconds=host.uptime)
  print("uptime: %dd%dh%dm%ds" % (uptime.day-1, uptime.hour, uptime.minute, uptime.second))
  print("addresses:")
  for (mac_address, ip_address) in host.addresses:
    print("    hwaddr: %s, ipv4 : %s" % (mac_address, ip_address))

def print_host(host):
  print("%-5s  %-17s  %-15s  %s '%s'" % ("eth?", host.hwaddr, host.ipv4, host.product, host.hostname))

# Unpack the header to get the version and overall message length.
host = namedtuple('Host', ["interface", "hwaddr", "ipv4", "addresses", "fwversion", "uptime", "hostname", "product"])
host.addresses = []

data = bytearray(1024)
(_, (host.ipv4,_)) = sock.recvfrom_into(data, 1024)
version, length = struct.unpack_from("!BxH", data, 0)
offset = struct.calcsize("!BxH")

# Iterate through the fields and parse each one out. 
while offset < length:
  field_id, field_length = struct.unpack_from("!BH", data, offset)
  offset += struct.calcsize("!BH")
  field_data = data[offset:offset+field_length]
  offset += field_length

  if field_id == fields["hwaddr"]:
    host.hwaddr = "%2X:%2X:%2X:%2X:%2X:%2X" % struct.unpack("!6B", field_data)
  if field_id == fields["address"]:
    mac_address = "%2X:%2X:%2X:%2X:%2X:%2X" % struct.unpack("!6B", field_data[:6])
    ip_address  = "%d.%d.%d.%d" % struct.unpack("!4B", field_data[6:])
    host.addresses.append((mac_address, ip_address))
  if field_id == fields["fwversion"]:
    host.fwversion = field_data
  if field_id == fields["uptime"]:
    host.uptime = struct.unpack("!L", field_data)[0]
  if field_id == fields["hostname"]:
    host.hostname = field_data
  if field_id == fields["product"]:
    host.product = field_data

print ("Local  Hardware Address   IP Address       Name")
print ("-----  -----------------  ---------------  -------------------")
print_host(host)
